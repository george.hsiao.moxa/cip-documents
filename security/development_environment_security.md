# <Center>CIP Development Environment Security </Center>

# Table of contents
1. [Objective](#Objective)
2. [Assumptions](#Assumptions)
3. [Scope](#Scope)
4. [Security Requirement](#Security_Requirement)
5. [CIP Development Environment](#CIP_Dev_Env)

   5.1 [Protection During Development](#Protec_Dev)
   
   5.2 [Protection During Production](#Protec_Prod)
   
   5.3 [Protection During Delivery](#Protec_Delivery)
   
6. [Policy for CIP repository maintainer privilege](#CIP_policy_maintainer)
7. [Current CIP repositories and maintainers](#CIP_repo_maintainer)
8. [Cloud Account Policy](#CIP_Cloud_Account)
9. [Reproducible CIP builds for integrity  
    verification](#CIP_Reproducible_Builds)


   ***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                       | Author       | Reviewed by                              |
|-------------|------------|------------------------------------------|--------------|------------------------------------------|
| 001         | 2021-06-14 | Draft development environment security   | Dinesh Kumar | To be reviewed by CIP Security WG members |
| 002         | 2021-06-19 | Updated based on Daniel-san's comment, email dated 15/06/2021 | Dinesh Kumar | To be reviewed by CIP Security WG members |
| 003         | 2021-08-30 | Updated based on Yasin's comment in gitlab | Dinesh Kumar | Yasin                                    |





****
<div style='page-break-after: always'></div>

















<div style='page-break-after: always'></div>

***

## 1. Objective <a name="Objective"></a>

The primary objective of this document is to document current development environment security, development flow and how security is maintained.

Moreover, subsequent revisions of this document may consider additional details of existing development or changes and enhancement to improve development environment security.

## 2. Assumptions <a name="Assumptions"></a>

| Assumption                               | Impact                                   |
|------------------------------------------|------------------------------------------|
| The development environment of upstream developers(Debian and Mainline kernel) is protected | Any security loopholes embedded in upstream project will impact CIP directly |


## 3. Scope <a name="Scope"></a>

Scope of this document is to consider current development model for CIP. Current CIP development model follows open source development method where everyone is allowed to contribute and only few members have privilege to merge the changes in CIP repositories.

## 4. Security Requirement <a name="Security_Requirements"></a>

IEC-62443-4-1 has following development environment security requirements

* CIP shall define process which has procedural as well as technical control for   protecting a product during development, production and delivery. This    
  includes following
  
  1. Update patches
  2. Design
  3. Implementation
  4. Testing
  5. Releases
   
* Having this process in place means CIP provides ways to protect integrity of  
  following
  
  1. Code
  2. Documents (User manuals, Design, )
  3. Configuration setting
  4. Private keys
  5. Authenticators (password, access control list, code signing certificates)

## 5. CIP Development Environment <a name="CIP_Dev_Env"></a>

### 5.1 Protection During Development <a name="Protec_Dev"></a>

As CIP re-uses open source components hence it is assumed upstream components are protected by respective component owner. However, artifacts such as various documents are kept in gitlab and they are protected by gitlab authentication mechanism. Similarly meta-data of recipes(.bb, .bbclass files etc) is protected by gitlab authentication mechanism.

During development any CIP developers can send merge requests with their changes. Here one thing to note is CIP developers can send merge requests, whereas CIP contributors can only send patches to the mailing list. All changes/patches are reviewed by CIP peer developers and feedback is provided. Once all the review comments are fixed, CIP maintainer of the respective repository merges the changes. 

![CIP development flow](../resources/images/other_documents/CIP_Dev_Flow.png)

### 5.2 Protection During Production <a name="Protec_Prod"></a>
### 5.3 Protection During Delivery <a name="Protec_Delivery"></a>
This requirement should be met by CIP users.(**TO_BE_MET_BY_CIP_USERS**)

Currently there is no production by CIP. When CIP adds production of images there will be information added here. Currently CIP does only deliver source code via Gitlab. Therefore, CIP relies on Gitlabs security

## 6. Policy for CIP repository maintainer privilege <a name="CIP_policy_maintainer"></a>

CIP has following policy for reviewing maintainers privilege to control CIP repositories

* If a CIP maintainer leaves CIP development and has not contributed in 6 months, his merge privilege is revoked in all CIP repositories and downgraded to developer privilege.
  
* All repositories are reviewed annually. If a maintainer has not contributed in 6 months, his maintainer privileges are revoked.
  
* Any CIP member can be given maintainer rights for any repositories after TSC members approval.

## 7. Current CIP repositories and maintainers <a name="CIP_repo_maintainer"></a>

| CIP Repositery | Repo URL                                 | Maintainer                               | Last review date |
|----------------|------------------------------------------|------------------------------------------|------------------|
| cip-security   | iec_62443-4-x <br><br>https://gitlab.com/cip-project/cip-security/iec_62443-4-x | Jan Kiszka, Chris Paterson, Daniel Sangorrin, Hidehiro Kawai, Kento Yoshida, Laurence Urhegyi, SZ Lin, Takehisa Katayama, Yoshitake Kobayashi | TBD              |
| cip-sw-update  | cip-sw-updates-demo <br><br>https://gitlab.com/cip-project/cip-sw-updates/cip-sw-updates-demo | Akihiro Suzuki, Jan Kiszka, Chris Paterson, Daniel Sangorrin, Hidehiro Kawai, Kento Yoshida, Laurence Urhegyi, SZ Lin, Takehisa Katayama, Yoshitake Kobayashi | TBD              |
| cip-core       | isa-cip-core <br><br>https://gitlab.com/cip-project/cip-core/isar-cip-core  <br><br>deby <br><br>https://gitlab.com/cip-project/cip-core/deby <br><br>cip-pkglist <br><br>https://gitlab.com/cip-project/cip-core/cip-pkglist | Jan Kiszka, Chris Paterson, Daniel Sangorrin, Hidehiro Kawai, Kazuhiro Hayashi, Kento Yoshida,  Laurence Urhegyi, SZ Lin, Takehisa Katayama, Yoshitake Kobayashi | TBD              |
| cip-kernel     | cip-kernel-sec <br><br>https://gitlab.com/cip-project/cip-kernel/cip-kernel-sec<br>linux-cip <br><br>https://gitlab.com/cip-project/cip-kernel/linux-cip <br><br>cip-kernel-config <br><br>https://gitlab.com/cip-project/cip-kernel/cip-kernel-config <br><br>lts-commit-list <br><br>https://gitlab.com/cip-project/cip-kernel/lts-commit-list | Jan Kiszka, Chris Paterson, Daniel Sangorrin, Hidehiro Kawai, Kazuhiro Hayashi, Kento Yoshida, Nobuhiro Iwamatsu, Pavel Machek, Laurence Urhegyi, SZ Lin, Takehisa Katayama, Yoshitake Kobayashi | TBD              |
| cip-testing    | cip-testing <br><br>https://gitlab.com/cip-project/cip-testing | Jan Kiszka, Chris Paterson, Daniel Sangorrin, Hidehiro Kawai, Kazuhiro Hayashi, Kento Yoshida,  Laurence Urhegyi, SZ Lin, Takehisa Katayama | TBD              |
| cip-lifecycle  | cip-lifecycle <br><br>https://gitlab.com/cip-project/cip-lifecycle | Jan Kiszka, Chris Paterson, Daniel Sangorrin, Hidehiro Kawai, Kazuhiro Hayashi, Kento Yoshida,  Laurence Urhegyi, SZ Lin, Takehisa Katayama | TBD              |
| cip-documents  | cip-documents<br><br>https://gitlab.com/cip-project/cip-documents | Jan Kiszka, Chris Paterson, Daniel Sangorrin, Dinesh Kumar, Hidehiro Kawai, Kento Yoshida,  Laurence Urhegyi, SZ Lin, Takehisa Katayama, Yoshitake Kobayashi | TBD              |

## 8. Cloud Account Policy <a name="CIP_Cloud_Account"></a>

CIP primarly uses AWS accounts for keeping CIP kernel images and other artifacts. Following is the detail of AWS account owners

* TBD

## 9. Reproducible CIP builds for integrity verification <a name="CIP_Reproducible_Builds"></a>

In future CIP plans to make CIP builds and image creation process reproducible.
This will ensure integrity of all CIP meta-data for image creation as well as process integrity remains intact.

This section will be updated once CIP achieves reproducible builds.
