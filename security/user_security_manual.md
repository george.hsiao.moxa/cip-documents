﻿# <Center>User Security Manual </Center>

# Table of contents
1. [Objective](#Objective)
2. [Assumptions](#Assumptions)
3. [Scope](#Scope) 
4. [Guidelines](#guidelines)


   ***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description         | Author       | Reviewed by                              |
|-------------|------------|----------------------------|--------------|------------------------------------------|
| 001         | 2021-09-30 | Draft User Security Manual | Dinesh Kumar | To be reviewed by CIP Security WG members |
|             |            |                            |              |                                          |





****
<div style='page-break-after: always'></div>

















<div style='page-break-after: always'></div>

***

## 1. Objective <a name="Objective"></a>

This document contains items identified during IEC-62443-4-1 and IEC-62443-4-2 Gap Assessment for user security manual. It should contain following items and is subject to revise based on Certification Body feedback or any other inputs.

* How to operate CIP in secure manner 
* CIP security configurations
* CIP users privileges to operate or configure the end products

This document is subject to revisions based on new findings or investigations in furture.

## 2. Assumptions <a name="Assumptions"></a>

| Assumption                               | Impact                                   |
|------------------------------------------|------------------------------------------|
| All documented guidelines are strictly followed by CIP users | These are recommendations identified from IEC-62443-4-2 & IEC-62443-4-1 investigation, not following these guidelines indicate non compliance of the product to IEC. |


## 3. Scope <a name="Scope"></a>

This document covers requirements from IEC-62443-4-1 & IEC-62443-4-2, where we have received gap assessment comments from Certification Body.

## 4. Guidelines <a name="guidelines"></a>

### 4.1 IEC-62443-4-2 (EDR-3.2) Protection from malicious code

CIP platform does not meet this requirement completely hence CIP users are advised to do risk assessment for the end product and find out suitable means to deploy for mitigating the risk by installing malicious code on the device. Some of the important areas to consider for protection includes.

* USB host access
* Detect integrity violation of application binaries and data files

Depending upon the end product barious techniques can be adopted e.g. no execeute bit (NX), data execution prevention (EDP), address space layout randomization (ASLR), stack corruption detection etc.

### 4.2 IEC-62443-4-2 (NDR-3.2) Protection from malicious code

CIP platform does not support this requirement and hence CIP user should consider the use of whitelisting or signing the software binaries in order to meet this requirement.

Other options could be to use suitable IPS/IDS packages provided by CIP platform.

### 4.2 IEC-62443-4-2 (CR-7.1) Denial of service protection

CIP platform meets this requirement by providing several security packages for ensuring device security. However, denial of service protection is very wide and requires measures to be taken by CIP end users as well.

CIP users should do CRT(Communication Robustness Testing) testing on end devices to confirm device meets robustness testing requirements.

### 4.3 IEC-62443-4-2 (CR-7.3) Control System Backup

CIP members are investigating to include suitable package which can support this requirement. However, CIP users need to configure data which need to be taken as remote backup.

In addition to taking backup CIP users should also check and confirm data can be restored on regular basis.