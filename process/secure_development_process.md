 # <Center>CIP Secure Development Process </Center>

# Table of contents
1. [Overview](#Overview)
2. [SM-1 Secure Development Process](#SEC_DEV_PROCESS)

    2.1 [Configuration Management](#CONFIG_MANAGEMENT)
    
    2.2 [Requirements definition](#REQ_DEF)
    
    2.3 [Architecture and Design](#ARCH_DESIGN)
    
    2.4 [Implementation](#IMPLEMENTATION)
    
    2.5 [Testing](#TESTING)
    
3. [SM-2 Identification of Responsibilities](#RACI_MATRIX)
4. [SM-3 CIP Software version](#CIP_SW_VER)
5. [SM-4 CIP Developer Security Expertise](#DEV_SEC_EXPERTISE)
6. [SM-5 Process Scoping](#PROCESS_SCOPING)
7. [SM-6 File Integrity](#FILE_INTEGRITY)
8. [SM-7 Development Environment Security](#DEV_ENV_SECURITY)
    
    8.1 [Development Security](#DEV_SECURITY)
    
    8.2 [Production Time Security](#PROD_SECURITY)
    
    8.3 [Delivery Time Security](#DELIVERY_SECURITY)
    
9. [SM-8 Private Key Protection](#PRIV_KEY_PROT)
10. [SM-9 Security Risk for new or externally provided  
     components](#SEC_RISK_EXT_COMP)
11. [SM-10 Custom Developed Components from third party](#CUSTOM_DEVELOPMENT)
12. [SM-11 Security Issues Assessment](#SEC_ISSUES_ASSESSMENT)
13. [SM-12 Documented Checklist Review](#CHECKLIST_REVIEW)
14. [SM-13 Define Review frequency ](#REVIEW_FREQUENCY)
15. [SR-1, SR-3, SR-4 Product Security Context](#PRODUCT_SEC_CONTEXT)
16. [SR-2 Threat Model ](#THREAT_MODEL)
17. [SR-5 Security Requirements Review and Approval](#SECURITY_REQUIREMENTS_REVIEW)
18. [SD-1 Secure Design Principles](#SECURE_DES_PRINCIPLE)
19. [SD-2 Defense in depth design](#DEFENSE_IN_DEPTH)
20. [SD-3, SD-4 Security design review](#SECURITY_DESIGN_REVIEW)
21. [SI-1, SI-2 Security implementation review](#SECURITY_IMPLEMENTATION_REVIEW)
22. [SVV-1 Security requirement testing](#SECURITY_REQUIREMENT_TESTING)
23. [SVV-2 Threat Mitigation testing](#THREAT_MITIGATION_TESTING)
24. [SVV-3 Vulnerability testing](#VULNERABILITY_TESTING)
25. [SVV-4 Penetration testing](#PENETRATION_TESTING)
26. [SVV-5 Independence of testers](#INDEPENDENCE_TESTERS)
27. [DM-1 to DM-5 Receiving notifications of security issues](#NOTIFICATION_OF_SECURITY_ISSUES)
28. [DM-6 Periodic review of security defect management practice](#REVIEW_DEFECT_MANAGEMENT)
29. [SUM-1 Security Update Qualification](#SECURITY_UPDATE_QUALIFICATION)
30. [SUM-2, SUM-3 Security update documentation](#SECURITY_UPDATE_DOCUMENTATION)
31. [SUM-4 Security update delivery](#SECURITY_UPDATE_DELIVERY)
32. [SUM-5 Timely delivery of security patches](#TIMELY_DELIVERY_SECURITY_PATCHES)
33. [SG-1, SG-2 Product defense in depth](#PRODUCT_DEFENSE_IN_DEPTH)
34. [SG-3 Security Hardening guidelines](#SECURITY_HARDENING_GUIDELINES)
35. [SG-4 Security disposable guidelines](#SECURITY_DISPOABLE_GUIDELINES)
36. [SG-5 Secure operation guidelines](#SECURITY_OPERATION_GUIDELINES)
37. [SG-6 Account management guidelines](#ACCOUNT_MANAGEMENT_GUIDELINES)
38. [SG-7 Documentation Review](#DOCUMENTATION_REVIEW)
39. [Acronyms](#ACRONYMS)
40. [References](#REFERENCES)
41. [Further Pending Items](#PENDING_ITEMS)






   ***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                       | Author       | Reviewed by |
|-------------|------------|------------------------------------------|--------------|-------------|
| 001         | 2021-03-14 | Draft secure development process         | Dinesh Kumar | TBR         |
| 002         | 2021-08-12 | Added about CVE tracking process in CIP kernel and CIP Core | Dinesh Kumar | TBR         |
| 003         | 2021-09-03 | Added reference for File Integrity document | Dinesh Kumar | TBR         |





****
<div style='page-break-after: always'></div>

















<div style='page-break-after: always'></div>

## 1. Overview <a name="Overview"></a>

This document is based on IEC-62443-4-1 (Edition 1.0 2018-01) secure development process requirements.The Objective is to adhere IEC-62443-4-1 secure development process requirements in CIP development as much as possible.

Adherence to these secure development practices will give an edge to CIP over other distributions at the same time it will reduce IEC-62443-4-x development effort for CIP member companies for making products based on CIP.

## 2. [SM-1] Secure Development Process <a name="SEC_DEV_PROCESS"></a>

CIP maintains several development artifacts to meet Secure Development Practices as defined by IEC-62443-4-1. Following artifacts are available for CIP development

### 2.1 Configuration Management <a name="CONFIG_MANAGEMENT"></a>

TODO: Add further links to configuration management.

### 2.2 Requirements definition <a name="REQ_DEF"></a>

TODO: Add link to CIP Requirements and CIP Security requirements

### 2.3 Architecture and Design <a name="ARCH_DESIGN"></a>

TODO: Add process for CIP architecture and design

### 2.4 Implementation <a name="IMPLEMENTATION"></a>

TODO: Add process for how implementation details are maintained

### 2.5 Testing <a name="TESTING"></a>

TODO: Add CIP Testing process followed

## 3. [SM-2] Identification of Responsibilities <a name="RACI_MATRIX"></a>

CIP has defined roles and responsibilities for the members who are responsible for CIP development. This RACI(Responsible, Accountable, Consulted and informed ) is reviewed and updated yearly once or whenever there is change in responsibilities

Detailed RACI MATRIX is available at 
TODO: ADD RACI Matrix link

## 4. [SM-3] CIP Software version <a name="CIP_SW_VER"></a>

TODO: Define CIP Core and CIP Kernel versions

## 5 [SM-4] CIP Developer Security Expertise <a name="DEV_SEC_EXPERTISE"></a>

TODO: Provide link for security expertise document

## 6 [SM-5] Process Scoping <a name="PROCESS_SCOPING"></a>

TODO: Add links to the document which has details of met and unmet requirements

## 7. [SM-6] File Integrity <a name="FILE_INTEGRITY"></a>

Following document explains about CIP File Integrity and how user can verify integrity of CIP deliverables.

[About CIP File Integrity](https://gitlab.com/cip-project/cip-documents/-/blob/master/process/file_integrity.md)

## 8. [SM-7] Development Environment Security <a name="DEV_ENV_SECURITY"></a>

### 8.1 Development Security <a name="DEV_SECURITY"></a>

Development environment security is achieved by providing restricted privileges to developers as well as all developers use certificate based authentication used by gitlab.

### 8.2 Production Time Security <a name="PROD_SECURITY"></a>

This requirement is not applicable to CIP.

### 8.3 Delivery Time Security <a name="DELIVERY_SECURITY"></a>

TODO: need to be discussed and documented.

## 9. [SM-8] Private Key Protection <a name="PRIV_KEY_PROT"></a>

CIP maintains document which explains about CIP Private key management. Please refer following document for more details.

[CIP Private Key Management](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/private_key_management.md)

## 10. [SM-9] Security Risk for new or externally provided components <a name="SEC_RISK_EXT_COMP"></a>


TODO: Define process for doing risk assessment for externally developed components

## 11. [SM-10] Custom Developed Components from third party<a name="CUSTOM_DEVELOPMENT"></a>

This is applicable to end products not CIP.

## 12. [SM-11] Security Issues Assessment <a name="SEC_ISSUES_ASSESSMENT"></a>

 CIP members to ensure all critical security issues are fixed before issuing a release.
 
 The Upstream vulnerability scanner will identify known security issues with CVE's and also log other issues in the bug tracking system. However, it may be required to do some CIP specific bug tracking if all the bugs cannot be tracked Upstream."

## 13. [SM-12] Documented Checklist Review <a name="CHECKLIST_REVIEW"></a>
 
 CIP will need a documented checklist for tracking security practices defined by IEC62443-4-1 along with a documented process for ensuring the checklist is reviewed and updated for each release
 
 TODO: This needs to be further discussed within CIP members, how to address this requirement. 

## 14. [SM-13] Define Review frequency <a name="REVIEW_FREQUENCY"></a>
 
 All development process artifacts should be reviewed once in a year.
 The review should cover following items and review comments and observations should be documented based on IEC-62443-4-1 requirements of review evidence.
 1. Issues in current development process.
 2. Any critical issues reported by CIP members
 3. Actions to be taken to improve on points #1 and #2

 
 ## 15. [SR-1, SR-3, SR-4] Product Security Context <a name="PRODUCT_SEC_CONTEXT"></a>
 
 CIP generic security context has been defined in Security Requirement document.
 The Security Context would be revised as and when new deployment scenarios and requirements are found.
 
 [CIP Security Requirements](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/security_requirements.md)
 
 ## 16. [SR-2] Threat Model <a name="THREAT_MODEL"></a>
 
 CIP generic Threat Model has been created which defines the condition of Threat Model Review and update frequency.
 
 Threat Model document is available at [CIP Threat Model document](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/threat_modelling.md)
 
 ## 17. [SR-5] Security Requirements Review and Approval <a name="SECURITY_REQUIREMENTS_REVIEW"></a>
 
 Security requirements should be reviewed and approved when created. All review comments should be documented. During review following members should be invited.
 
 *  Architects/developers (those who will implement the requirements)
 *  Testers (those who will validate that the requirements have been met)
 *  Customer advocate (such as sales, marketing, product management or
    customer support) and Security Adviser
	 
 ## 18. [SD-1] Secure Design Principles <a name="SECURE_DES_PRINCIPLE"></a>
 
  1. The design shows how the system's devices and subsystems are connected, and    how external actors are connected to the system.
   
  2. The design shows all protocols used by all external actors to communicate  
     with the system.
   
  3. Trust boundaries are documented.
   
  4. The design document should be updated whenever the design changes
  
  5. Document a CIP design description and document the following verification 
     The design identifies and describes the exposed interfaces (refer items a. – j.) from this Requirement Description.
 
 ## 19. [SD-2] Defense in depth design <a name="DEFENSE_IN_DEPTH"></a>
 
 Defense in depth design should be created by end product owners as it depends upon end products design and what kind of security layers would be part of the defense layers.
 
 This requirement is not applicable to CIP and should be met by end product owners.
 
 ## 20. [SD-3, SD-4] Security design review <a name="SECURITY_DESIGN_REVIEW"></a>
 
 * Create evidence for security design reviews
 * Issues identified during security design reviews are tracked using gitlab
 * Create Traceability matrix for security requirements to security design
 * Create Traceability matrix from threat mitigation to security design
 * Create Security guidelines for user
 * Include security design best practices used in debian as well as review   
   Design best practices being developed by OpenSSF if suitable include in CIP
   
 ## 21. [SI-1, SI-2] Security implementation review <a name="SECURITY_IMPLEMENTATION_REVIEW"></a>
 
 * Document Debian secure coding guidelines
 * Perform static code analysis or re-us from upstream for critical packages
 * Code reviews should document following information
 
	  1. Name of the person who performed the code review,
	  2. The date of the code review,
	  3. The results of the code review
	  4. The name of the person responsible for fixing problems identified in the    code   review
	  5. Date or indication that all problems were fixed.
 
 ## 22. [SVV-1] Security requirement testing <a name="SECURITY_REQUIREMENT_TESTING"></a>
 
 CIP Security requirements testing should be done to cover IEC-62443-4-1 testing requirements applicable to CIP, following are the IEC-62443-4-1 requirements
 
 * Functional testing of security requirements
 * Performance and scalability testing and Boundary/edge condition, stress and  
   malformed or unexpected input tests not specifically targeted at security.
 * General security capabilities (features);
 *  API (application programming interface);
 * Permission delegation;
 * Anti-tampering and integrity functionality;
 * Signed image verification; and
 * Secure storage of secrets.
 
  ## 23. [SVV-2] Threat Mitigation testing <a name="THREAT_MITIGATION_TESTING"></a>
  
  Create a Traceability matrix to show each threat has mitigation and test to verify the mitigation
  
  ## 24. [SVV-3] Vulnerability testing <a name="VULNERABILITY_TESTING"></a>
  
  Vulnerability scanner and Pen testing Tool should be used and following testing should be done.
  
  * Network storm testing should be done to simulate DOS attacks
  * Software composition analysis must be done against the binaries
  * Attack surface analysis
  * Known vulnerability scanning
  * Dynamic Runtime Resource Analysis Testing
  * Fuzz testing on all protocols sent externally should be included as part of   this testing
  
  ## 25. [SVV-4] Penetration testing <a name="PENETRATION_TESTING"></a>
  
  Penetration testing should be conducted by using some suitable tool.
  
  ## 26. [SVV-5] Independence of testers <a name="INDEPENDENCE_TESTERS"></a>
  
  Document testers involved in CIP testing and ensure testers are different than developers.
  
  ## 27. [DM-1 to DM-5] Receiving notifications of security issues <a name="NOTIFICATION_OF_SECURITY_ISSUES"></a>
  
  Security issues are tracked using CVE scanner tools for both CIP Kernel and CIP Core.
  
  CIP CVE scanner runs periodically to fetch fixes for CVEs and apply in CIP repos.

  Further details can be found about CIP Core CVE scanner at [CIP Core CVE scanner](https://gitlab.com/cip-playground/cip-core-sec)
  
  **CIP Kernel CVE Scanner**
  
  CIP Kernel CVE checking is done weekly and reports are published in cip-dev mailing list.) Currently there is no specific policy to stop releasing because of missing patches as long as stable kernels are released
  
  Release policy is reported at every E-TSC. The latest one is as follows
  [CIP Kernel CVE fixes release policy](https://docs.google.com/presentation/d/12cP80przQxXkzj2fAUptnieHar0jrB00vzIePkh5kwo/edit#slide=id.g9f78cf691e_0_155)
  
  Further details of CIP Kernel CVE scanner can be found at [CIP Kernel CVE scanner](https://gitlab.com/cip-project/cip-kernel/cip-kernel-sec)
  
  Notification of CVE fixes are sent by email to CIP users.
  
  CIP does not maintain it's own bug tracking system.
  
  ## 28. [DM-6] Periodic review of security defect management practice <a name="REVIEW_DEFECT_MANAGEMENT"></a>
  
  Review current defect management practices and processes once in a year and make required changes as needed.
  
  ## 29. [SUM-1] Security Update Qualification <a name="SECURITY_UPDATE_QUALIFICATION"></a>
  
  CIP can not produce evidence for details of testing each patch or security issues. The verification information is not kept at any central location, it's scattered at multiple locations such as KernelCI, LAVA as well as mailing list.

  CIP has LAVA automated tests which are executed when some code changes are merged. At the moment there is no tests for confirming side effects.
  
  ## 30. [SUM-2, SUM-3] Security update documentation <a name="SECURITY_UPDATE_DOCUMENTATION"></a>
  
  CIP will continue to use mailing as main channel for sharing security issues information with all users.
   
  ## 31. [SUM-4] Security update delivery <a name="SECURITY_UPDATE_DELIVERY"></a>
  
  CIP releases patches, CIP kernel and CIP Core meta-data which are signed by CIP developers.
  
  
  ## 32. [SUM-5] Timely delivery of security patches <a name="TIMELY_DELIVERY_SECURITY_PATCHES"></a>
  
  TBD: Document the frequency of CIP releases.
  
  ## 33. [SG-1, SG-2] Product defense in depth <a name="PRODUCT_DEFENSE_IN_DEPTH"></a>
  
  This process is not applicable to CIP, Defense in depth should be done by end products owner.
  
  ## 34. [SG-3] Security Hardening guidelines <a name="SECURITY_HARDENING_GUIDELINES"></a>
  
  CIP has defined security hardening guidelines for following.
  * Default security policies to meet IEC-62443-4-1 security requirements
  * Compilation flags
  * Other configs
  
  ## 35. [SG-4] Security disposable guidelines <a name="SECURITY_DISPOABLE_GUIDELINES"></a>
  
  Disposable guidelines are applicable to end products and not applicable to CIP.
  
  ## 36. [SG-5] Secure operation guidelines <a name="SECURITY_OPERATION_GUIDELINES"></a>
  
  Following operation guidelines should be documented.
  
  * Security Updates
  * Monitoring system logs
  * IPS and IDS monitoring logs
  
  ## 37. [SG-6] Account management guidelines <a name="ACCOUNT_MANAGEMENT_GUIDELINES"></a>
  
  CIP users can create CIP security image which has security packages to meet IEC-62443-4-2 security requirements, follow the instructions to create [CIP security image](https://gitlab.com/cip-project/cip-core/isar-cip-core), choose security extensions option in the kas menu.
  
  CIP security image has only one default user account which is root account. For additional user accounts, follow standard linux user management steps.
  
  ## 38. [SG-7] Documentation Review <a name="DOCUMENTATION_REVIEW"></a>
   
  All CIP development documents are maintained in gitlab. Reviewers can give comments using any of the following methods.
  
  * Create gitlab issues with comments
  * Send review comments in email
  * Send MR with the changes
  
  Only few CIP members have rights to merge review comments changes in the documents.
  
  ## 39. Acronyms <a name="ACRONYMS"></a>
  
| Acronym | Description                           |
|---------|---------------------------------------|
| SM      | Security Management                   |
| SR      | Specification of security requirement |
| SD      | Secure by design                      |
| SI      | Secure implementation                 |
| SVV     | Security verification and validation  |
| DM      | Defect Management                     |
| SUM     | Security update management            |
| SG      | Security guidelines                   |
| CIP     | Civil Infrastructure Platform         |
| IPS     | Intrusion detection system            |
| IDS     | Intrusion detection system            |

  
  ## 40. References <a name="REFERENCES"></a>
  
  | Item          | Reference                                |
|---------------|------------------------------------------|
| IEC-62443-4-1 | https://webstore.iec.ch/preview/info_iec62443-4-1%7Bed1.0%7Den.pdf |

  
  ## Further pending items <a name="PENDING_ITEMS"></a>
  
  * Create AIs in gitlab for TODO items
  * All members need to review and share comments from evidence perspective
  * Fix formatting issues
  * Add Back to Top button
